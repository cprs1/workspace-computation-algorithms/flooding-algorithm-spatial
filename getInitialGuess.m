%% FLOODING ALGORITHM - Spatial CASE
% F.Zaccaria 02 February 2022
% extension to the spatial case of the algorithm provided by "Singularity
% Conditions for Continuum Parallel Robot"

function y0 = getInitialGuess(WK,guesses,idmentry)
[~,idmin] = min(WK(idmentry,10));
idm = idmentry(idmin);
y0 = guesses(:,idm);
end