%% FLOODING ALGORITHM - Spatial CASE
% F.Zaccaria 02 February 2022
% extension to the spatial case of the algorithm provided by "Singularity
% Conditions for Continuum Parallel Robot"

%% MAIN FILE
clear
close all
clc

%% ROBOT PARAMETERS
FILE_6RFSrobotfile

%% SIMULATION PARAMETERS
FILE_inputfile

%%
instantplot = 0;
[WK,outstruct] = FCN_FloodingSpatialWK(fcn,params,instantplot);

%%

FCN_plot(WK,params)
