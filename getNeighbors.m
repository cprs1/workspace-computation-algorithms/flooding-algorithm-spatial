%% FLOODING ALGORITHM - Spatial CASE
% F.Zaccaria 02 February 2022
% extension to the spatial case of the algorithm provided by "Singularity
% Conditions for Continuum Parallel Robot"

function id2 = getNeighbors(WK,pstart,params,fact)
stepsize = params.stepsize;
id = ((abs(WK(:,2)-pstart(1))<=fact*stepsize) & (abs(WK(:,3)-pstart(2))<=fact*stepsize) & (abs(WK(:,4)-pstart(3))<=fact*stepsize));
id2 = WK(id==1,1);
end