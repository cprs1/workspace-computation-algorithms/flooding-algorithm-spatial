%% Robot Geometry Parameters

rb = 0.3;
rp = 0.2;
dp = 0.1;

th1 =   0*pi/180;
th2 = 060*pi/180;
th3 = 120*pi/180;
th4 = 180*pi/180;
th5 = 240*pi/180;
th6 = 300*pi/180;

baseangles = [th1;th2;th3;th4;th5;th6] + pi/2;

A1 = Rz(th1)*[rb;0;0]; % WRT base frame
A2 = Rz(th2)*[rb;0;0];
A3 = Rz(th3)*[rb;0;0];
A4 = Rz(th4)*[rb;0;0];
A5 = Rz(th5)*[rb;0;0];
A6 = Rz(th6)*[rb;0;0];

basepoints = [A1,A2,A3,A4,A5,A6];


thp1 = 030*pi/180;
thp2 = 150*pi/180;
thp3 = 270*pi/180;

p1 = Rz(thp1)*[rp;0;0]; % WRT platform frame
p2 = Rz(thp2)*[rp;0;0];
p3 = Rz(thp3)*[rp;0;0];

p1B1 = +Rz(thp1-pi/2)*[dp;0;0];
p1B2 = +Rz(thp1+pi/2)*[dp;0;0];
p2B3 = +Rz(thp2-pi/2)*[dp;0;0];
p2B4 = +Rz(thp2+pi/2)*[dp;0;0];
p3B5 = +Rz(thp3-pi/2)*[dp;0;0];
p3B6 = +Rz(thp3+pi/2)*[dp;0;0];

B1 = p1+p1B1;
B2 = p1+p1B2;
B3 = p2+p2B3;
B4 = p2+p2B4;
B5 = p3+p3B5;
B6 = p3+p3B6;

platformpoints = [B1,B2,B3,B4,B5,B6];

rotparams = 'XYZ';
geometry.basepoints = basepoints;
geometry.platpoints = platformpoints;
geometry.baseangles = baseangles;
geometry.rotparams = rotparams;

%% Robot Material Parameters
r = 0.001; % beams cross section radius [m]
A = pi*r^2;
I = 0.25*pi*r^4; % inertial moment of area [m^4]
L = 1;
J = 2*I;
E = 210*10^9; % young modulus [Pa]
EI = E*I; 
G = 80*10^9; % shear modulus [Pa]
Kbt = diag([EI;EI;G*J]);
stresslim = +Inf;

%% EXTERNAL LOADS
g = [0;0;0];
rho = 7800;
f = rho*A*g;
fext = [0;0;0];
l = zeros(3,1);
mext = [0;0;0];

wd = [l;f]; % distributed wrench
wp = [mext;fext]; % platform wrench


%% Model Parameters
Nf = 4;
Nleg = 6;
Phi = @(s) PhiMatr(s,1,Nf);
Kad = L*integral( @(s) Phi(s)'*Kbt*Phi(s),0,1,'ArrayValued',true);

roll = 0*pi/180;
pitch = 0*pi/180;
yaw = 0*pi/180;

hp = [roll,pitch,yaw]';

actlim = [-Inf*ones(Nleg,1),+Inf*ones(Nleg,1)];

pstart = [0;0;0.8];
params.pstart = pstart;


%% Simulation functions
fcn.objetivefcn = @(y,pend) InverseMode6RFS_mex(y,geometry,L,Kad,[pend;hp],wp,wd,Nf);
fcn.mechconstrfcn = @(y) mechconstr6RFS(y,actlim,r,E,Nf,Nleg,stresslim);
fcn.singufcn = @(y,jac) SingularityMode6RFS(y,jac,Nf,rotparams);
fcn.stabilityfcn = @(y,jac) StabilityMode6RFS(y,jac,Nf,rotparams);
fcn.internfcn = @(y) InternalEnergy6RFS(y,Kad,Nf);

%% First Initial Guess

qa0 = 15*pi/180 * ones(6,1);
A0 = [-1;zeros(3*Nf-1,1)];
qe0 = repmat(A0,Nleg,1);
qp0 = [pstart;hp];
lambda0 = 0.001*rand(3*Nleg,1);
y0 = [qa0;qe0;qp0;lambda0];

fun = @(y) InverseMode6RFS(y,geometry,L,Kad,[pstart;hp],wp,wd,Nf);
options = optimoptions('fsolve','display','iter-detailed','SpecifyObjectiveGradient',true,'CheckGradients',true);
[sol,~,~,~,jac] = fsolve(fun,y0,options);

%%
posbeams = pos6RFS(sol,geometry,Nf,L);

qa = sol(1:6,1);
pplat = sol(1+6+6*3*Nf:6+6*3*Nf+3,1);
[Rplat,~,~,~] = rotationParametrization(sol(1+6+6*3*Nf+3:6+6*3*Nf+6,1),rotparams);

h = Plot6RFS(geometry,qa,posbeams,pplat,Rplat);
drawnow
params.y0 = sol;
