function energy = InternalEnergy6RFS(y,Kee,Nf)
energy = 0;
qe = y(1+6:6+3*6*Nf,1);
for i = 1: 6
    qei = qe(1+3*Nf*(i-1):3*Nf*i,1);
    energy = energy + 0.5*qei'*Kee*qei;    
end