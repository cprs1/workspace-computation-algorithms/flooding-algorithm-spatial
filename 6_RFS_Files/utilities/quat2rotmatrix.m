% CONVERT QUATERNION HP INTO ROTATION MATRIX
% hP = h1 + h2 i + h3 j + h4 k (h1 = scalar component)
function Rp = quat2rotmatrix(hp)
    d1 = [hp(1)^2+hp(2)^2-hp(3)^2-hp(4)^2;2*(hp(1)*hp(4)+hp(2)*hp(3));2*(hp(2)*hp(4)-hp(1)*hp(3))];
    d2 = [2*(hp(2)*hp(3)-hp(1)*hp(4));hp(1)^2-hp(2)^2+hp(3)^2-hp(4)^2;2*(hp(1)*hp(2)+hp(3)*hp(4))];
    d3 = [2*(hp(1)*hp(3)+hp(2)*hp(4));2*(hp(3)*hp(4)-hp(1)*hp(2));hp(1)^2-hp(2)^2-hp(3)^2+hp(4)^2];
    Rp = [d1,d2,d3];
end

