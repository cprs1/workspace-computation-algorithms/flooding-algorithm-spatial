function flag = mechconstr6RFS(y,actlim,r,E,Nf,Nleg,stresslim)

lA = y(1:Nleg,1);
qe = y(1+Nleg:Nleg+3*Nf*Nleg,1);
N_check = 100;
flag = 1;
i=1;

while i <= Nleg && flag==1
    if lA(i)<actlim(i,1) || lA(i)>actlim(i,2)
        flag = 0;
    end
    s_vect = linspace(0,1,N_check);
    j=1;
    while (j <= N_check) && (flag==1)
        qei = qe(1+Nf*3*(i-1):Nf*3*i,1);
        Phi =  BaseFcnLegendre(s_vect(j),1,Nf);
        u1 = Phi'*qei(1+Nf*(1-1):Nf*1,1);
        u2 = Phi'*qei(1+Nf*(2-1):Nf*2,1);
        sigma_x = r*E*u1;
        sigma_y = r*E*u2;
        %         tau_z = 0 % TO CONSIDER TORSION and equivalent stress
        if sigma_x>stresslim || sigma_y>stresslim
            flag = 0;
        end
        j = j+1;
    end
    i = i+1;
end