# Flooding Algorithm - Spatial

Flooding algorithm for the computation of spatial workspaces of CPRs.

Last update by F.Zaccaria on 02 February 2022

Extension to the spatial case of the flooding algorithm of the paper "Singularity Conditions for Continuum Parallel Robots"