%% FLOODING ALGORITHM - Spatial CASE
% F.Zaccaria 02 February 2022
% extension to the spatial case of the algorithm provided by "Singularity
% Conditions for Continuum Parallel Robot"

%% INPUT FILE

%% Simulation parameters
gridflag = 'F';  %choose between 'F' (fixed) or 'V' (variable)
guessflag = 'C'; %choose between K (bestkantorovich) or C (bestconditioning)

maxiter = 20;
TOL = 10^-4;
TOL2 = 1800;
stepsize = 0.01;
boxsize = [-1.2 +1.2  -1.2 +1.2 -.2 +1];



%% STORE VARIABLES

params.stepsize = stepsize;
params.maxiter = maxiter;
params.boxsize = boxsize;
params.TOL = TOL;
params.TOL2 = TOL2;
params.gridflag = gridflag;
params.guessflag = guessflag;
params.solver = 'fsolve';


