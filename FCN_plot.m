%% FLOODING ALGORITHM - Spatial CASE
% F.Zaccaria 02 February 2022
% extension to the spatial case of the algorithm provided by "Singularity
% Conditions for Continuum Parallel Robot"

function FCN_plot(WK,parameters)

TOL = parameters.TOL;
TOL2 = parameters.TOL2;
boxsize = parameters.boxsize;

idwk = (WK(:,5) == 1 & WK(:,7) == 0);
idun = (WK(:,5) == 1 & WK(:,7) ~= 0);
idT1 = (WK(:,10)>TOL2);
idT2 = (WK(:,9)<TOL & WK(:,10)>0);
idlimits = (WK(:,5) == 2);

% figure()
plot3(WK(idwk,2),WK(idwk,3),WK(idwk,4),'b.')
hold on
plot3(WK(idun,2),WK(idun,3),WK(idun,4),'y.')
plot3(WK(idT2,2),WK(idT2,3),WK(idT2,4),'k.')
plot3(WK(idT1,2),WK(idT1,3),WK(idT1,4),'r.')
if numel(idlimits)>0
    plot3(WK(idlimits,2),WK(idlimits,3),WK(idlimits,4),'.','Color',[0, 0.5, 0])
end
xlabel('x_{[m]}')
ylabel('y_{[m]}')
zlabel('z_{[m]}')
grid on
axis equal
axis(boxsize)
title('Workspace')

end