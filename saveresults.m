%% FLOODING ALGORITHM - Spatial CASE
% F.Zaccaria 02 February 2022
% extension to the spatial case of the algorithm provided by "Singularity
% Conditions for Continuum Parallel Robot"

function WK_line = saveresults(y,jac,fcn)
Stability = fcn.stabilityfcn;
Singu = fcn.singufcn;
InternalEnergy = fcn.internfcn;

id_7 = Stability(y,jac); 
[id_8,id_9] = Singu(y,jac);
id_10 = norm(inv(jac),Inf); 
id_11 = InternalEnergy(y);
id_5_6 = [1,1];
                
WK_line = [id_5_6,id_7,id_8,id_9,id_10,id_11];

end